package demoApi

import (
	"context"
	"fmt"

	"git.zero.demo/api/internal/svc"
	"git.zero.demo/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type DemoLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewDemoLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DemoLogic {
	return &DemoLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *DemoLogic) Demo(req *types.DemoReq) (resp *types.DemoRsp, err error) {
	// todo: add your logic here and delete this line

	return nil, fmt.Errorf("12345678")
}
