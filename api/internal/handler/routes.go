// Code generated by goctl. DO NOT EDIT.
package handler

import (
	"net/http"

	demoApi "git.zero.demo/api/internal/handler/demoApi"
	"git.zero.demo/api/internal/svc"

	"github.com/zeromicro/go-zero/rest"
)

func RegisterHandlers(server *rest.Server, serverCtx *svc.ServiceContext) {
	server.AddRoutes(
		[]rest.Route{
			{
				Method:  http.MethodPost,
				Path:    "/demo",
				Handler: demoApi.DemoHandler(serverCtx),
			},
		},
	)
}
