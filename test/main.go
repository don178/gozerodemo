package main

import (
	"context"
	"log"

	"entgo.io/ent/dialect/sql/schema"
	"git.zero.demo/ent"
	"git.zero.demo/ent/intercept"
	_ "git.zero.demo/ent/runtime"
	_ "github.com/go-sql-driver/mysql"
	"github.com/zeromicro/go-zero/core/logx"
)

func main() {

	logx.SetUp(logx.LogConf{
		Level:    "debug",
		Encoding: "plain",
	})

	client, err := ent.Open(
		"mysql",
		"root:990430@tcp(127.0.0.1:3306)/test?parseTime=True",
		ent.Log(logx.Debug),
		ent.Debug(),
	)
	if err != nil {
		log.Fatalf("failed opening connection to mysql: %v", err)
	}
	defer client.Close()
	// Run the auto migration tool.
	if err := client.Schema.Create(context.Background(),
		schema.WithForeignKeys(false), schema.WithDropColumn(true),
	); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	ctx := context.Background()

	client.User.Intercept(
		intercept.Func(func(ctx context.Context, q intercept.Query) error {
			return nil
		}),
		intercept.TraverseCar(func(ctx context.Context, cq *ent.CarQuery) error {
			return nil
		}),
	)

	list, err := client.User.Query().All(ctx)

	logx.Must(err)
	logx.Info(list)

	// car, err := client.Car.Create().
	// 	SetModel("mode").
	// 	SetRegisteredAt(time.Now()).Save(ctx)

	// logx.Must(err)

	// user, err := client.User.Update().Where(user.ID(10)).ClearCars().Save(ctx)

	// logx.Must(err)
	// logx.Info(user)

}
